package it.uniroma2.pjdm.t9test0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class VocActivity extends AppCompatActivity {

    public static final String WORDKEY = "word";
    private String currentword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voc);

        /* get the word from the intent */
        Intent intent = getIntent();
        currentword = intent.getStringExtra(WORDKEY);
        currentword = currentword.toLowerCase();

        /* display the current word */
        TextView termTV = findViewById(R.id.word);
        termTV.setText(currentword);

        Log.d("CLA","current word: " + currentword);

        /* parse the JSON file */
        String json = "";
        try {
            InputStream is = getAssets().open("voc.json");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            json = new String(buffer, "UTF-8");
            is.close();

        } catch (IOException e) {
            Log.d("CLA", "problem opening file");
        }

        /* search the term */
        try {
            JSONArray ja = new JSONArray(json);
            for(int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                String term = jo.getString("term");
                if(term.equals(currentword)) {
                    /* term found */
                    TextView tv = findViewById(R.id.wordDefinition);
                    tv.setText(jo.getString("definition"));
                    /* make the textview scrollable */
                    tv.setMovementMethod(new ScrollingMovementMethod());
                    break;
                }
            }
        } catch (JSONException e) {
            Log.d("CLA", "JSON problem");
            e.printStackTrace();
        }
    }
}
