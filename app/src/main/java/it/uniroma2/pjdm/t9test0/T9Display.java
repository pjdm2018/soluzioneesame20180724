package it.uniroma2.pjdm.t9test0;

import android.widget.TextView;

/**
 * Created by clauz on 7/13/18.
 */

public class T9Display {
    private TextView displayTV;
    private String currentText = "";

    public static final char nullCharacter = '\u0000';

    public T9Display(TextView tv, String initialText) {
        displayTV = tv;
        currentText = initialText;
        update();
    }

    public void add(char newChar) {
        if(newChar == nullCharacter)
            return;

        currentText += newChar;
        update();
    }

    public void backSpace() {
        /* remove the last character */
        int tlen = currentText.length();
        if(tlen == 0)
            return;
        /* remove the last character */
        currentText = currentText.substring(0, tlen - 1);
        update();
    }

    public void replace(char newChar) {
        /* replace the last character */
        backSpace();
        add(newChar);
    }

    private void update() {
        displayTV.setText(currentText);
    }

    public char getLastChar() {
        // return the last character on the display
        int tlen = currentText.length();
        if(tlen > 0) {
            return currentText.charAt(currentText.length() - 1);
        } else {
            return nullCharacter;
        }
    }

    public void clear() {
        currentText = "";
        update();
    }

    public String getCurrentText() {
        return this.currentText;
    }
}
