package it.uniroma2.pjdm.t9test0;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private T9Display display;

    private char[][] keyboard = {
    /* 0 */ {},
    /* 1 */ {},
    /* 2 */ {'A', 'B', 'C'},
    /* 3 */ {'D', 'E', 'F'},
    /* 4 */ {'G', 'H', 'I'},
    /* 5 */ {'J', 'K', 'L'},
    /* 6 */ {'M', 'N', 'O'},
    /* 7 */ {'P', 'Q', 'R', 'S'},
    /* 8 */ {'T', 'U', 'V'},
    /* 9 */ {'W', 'X', 'Y', 'Z'},
    };

    private boolean replace = false;
    private boolean goToNext = false;
    private Handler g2nHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* check if we have something saved */
        String initialText = "";
        if(savedInstanceState != null && savedInstanceState.containsKey("displayText")) {
            initialText = savedInstanceState.getString("displayText");
        }

        /* initialize the display */
        TextView tv = findViewById(R.id.display);
        display = new T9Display(tv, initialText);

        /* emulate pressing the 0 button */
        g2nHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Button button = findViewById(R.id.but0);
                button.callOnClick();
            }
        };
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("displayText", display.getCurrentText());
    }

    public void buttonPressed(View view) {
        Button button = (Button) view;
        String name = button.getText().toString();
        Log.d("CLA", "you have pressed " + name);
        char firstChar = name.charAt(0);
        Log.d("CLA", "first char: " + firstChar);

        char lastChar = display.getLastChar();
        char nextChar = T9Display.nullCharacter;

        Log.d("CLA", "last character: " + lastChar);

        switch(firstChar) {
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                /* get the number of letters on the pressed key */
                int arrayIndex = Integer.valueOf("" + firstChar);
                int numberOfLetters = keyboard[arrayIndex].length;

                if(goToNext) {
                    nextChar = keyboard[arrayIndex][0];
                    replace = false;
                    goToNext = false;
                    break;
                }

                boolean matched = false;
                for(int i = 0; i < numberOfLetters; i++) {
                    char c = keyboard[arrayIndex][i];
                    if (c == lastChar) { /* we have a match */
                        matched = true;
                        if (i == numberOfLetters - 1) /* last character */
                            nextChar = keyboard[arrayIndex][0];
                        else
                            nextChar = (char) (lastChar + 1);
                        replace = true;
                        goToNext = false;
                        break;
                    }
                }

                /* a new key was pressed */
                if(!matched) {
                    nextChar = keyboard[arrayIndex][0];
                    replace = false;
                    goToNext = false;
                }

                break;
            case '#':
                display.clear();
                replace = false;
                goToNext = false;
                break;
            case '0':
                replace = false;
                goToNext = true;
                break;
            case '*':
                display.backSpace();
                replace = false;
                goToNext = true;
                break;
            case '1':
                /* launch activity */
                String currentword = display.getCurrentText();
                Log.d("CLA", "current word from display: " + currentword);
                Intent intent = new Intent(this, VocActivity.class);
                intent.putExtra(VocActivity.WORDKEY, currentword);
                startActivity(intent);
                return;
            default:
                Log.d("CLA", "not implemented");
        }

        if(replace && !goToNext)
            display.replace(nextChar);
        else
            display.add(nextChar);

        /* emulate pressing the 0 button after two seconds */
        g2nHandler.removeMessages(1);
        Message msg = g2nHandler.obtainMessage();
        msg.what = 1;
        g2nHandler.sendMessageDelayed(msg, 2000);
    }
}
